import { render, screen } from "@testing-library/react";
import App from "./App";
// ctrl + shift + p  = wallabyjs start

test("renders learn react link", () => {
  render(<App />);
  // getByText(/learn react/i); - uses regular expression so the text can be upper/lowercase
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test("renders 3 list items", () => {
  render(<App />);
  const listItems = screen.getAllByRole("listitem");
  // expect(listItems).toHaveLength(3);
  // expect(listItems.length).toBe(3);
  expect(listItems.length).toEqual(3);
});

test("renders title", () => {
  render(<App />);
  const title = screen.getByTestId("mytestid");
  expect(title).toBeInTheDocument();
});

test("sum should be 6", () => {
  render(<App />);
  const sum = screen.getByTitle("sum");
  expect(Number(sum.textContent)).toBe(6);
});
